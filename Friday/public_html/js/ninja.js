
/* jquery init */
$(document).ready(function(){
  // slick slider
  $('.slider-photos').slick({
    slidesToShow: 6,
    slidesToScroll: 6,
    responsive: [
      {
        breakpoint: 992, 
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4,
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        }
      }
    ]
  });
});
